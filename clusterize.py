# %%
# Load dataset
import datetime
import sys
from reader import tud_to_networkx
from augmentation.database import Database
from augmentation.in_ipynb import in_ipynb


def init(table, dataset=None):
    db = Database(table, user='phd', passwd='cMRwE79hG5BxTfmj',
                  db='graph_clustering_similarity', host="localhost", port=45432)
    db.create_table()
    print(datetime.datetime.now(), 'read dataset - start')
    if not dataset:
        dataset = table
    dataset = tud_to_networkx(dataset, './datasets/')
    print(datetime.datetime.now(), 'read dataset - end')
    db.disconnect()
    return dataset


if in_ipynb() and __name__ == "__main__":
    dataset = init()

# %%
# Make clusters
import datetime
from augmentation.database import Database
from augmentation.cluster import cluster_on_dataset
from augmentation.in_ipynb import in_ipynb


def calc_container(dataset, table):
    db = Database(table, user='phd', passwd='cMRwE79hG5BxTfmj', db='graph_clustering_similarity',
                  host="localhost", port=45432, use_pool=True, pool_name="cluster")
    print(datetime.datetime.now(), 'make clusters - start')
    data_container = None
    try:
        data_container = cluster_on_dataset(dataset, 5, db, try_load=True,
                                            update=True, verbose=False, use_threads=True)
    except Exception as err:
        print(datetime.datetime.now())
        raise err
    print(datetime.datetime.now(), 'make clusters - end')
    return data_container


if in_ipynb() and __name__ == "__main__":
    calc_container(dataset)

# %%
# Load one graph
from augmentation.database import Database
from augmentation.in_ipynb import in_ipynb


def load_data_from_db(graph_id, table):
    db = Database(table, user='phd', passwd='cMRwE79hG5BxTfmj', db='graph_clustering_similarity',
                  host="localhost", port=45432)
    data = db.get_graph_cluster(graph_id, 5, ['`graph`', '`subgraphs`'])
    db.disconnect()
    return data


if in_ipynb() and __name__ == "__main__":
    idx = input('Graph index to load: ')
    load_data_from_db(int(idx))

# %%
# Draw graph
from augmentation.draw import draw_adjacency_matrix
from augmentation.database import Database
from augmentation.cluster import group_clusters
from augmentation.in_ipynb import in_ipynb


def draw_graph(graph_id, n_clusters, table):
    db = Database(table, user='phd', passwd='cMRwE79hG5BxTfmj',
                  db='graph_clustering_similarity', host="localhost", port=45432)
    data = db.get_graph_cluster(graph_id, n_clusters, ['graph', 'num_clusters', 'clusters'])
    adj_matrix, _, _, counters = group_clusters(data)
    colors = ['red']
    draw_adjacency_matrix(data['graph'])
    draw_adjacency_matrix(adj_matrix, partitions=[[c[2] for c in counters]], colors=colors)
    db.disconnect()


if in_ipynb() and __name__ == "__main__":
    idx = input('Graph index to draw: ')
    draw_graph(int(idx), 5)

# %%
# Organize graphs
from augmentation.database import Database
from augmentation.cluster import group_all_clusters
from augmentation.in_ipynb import in_ipynb


def organize_graphs(table, verbose=False):
    db = Database(table, user='phd', passwd='cMRwE79hG5BxTfmj',
                  db='graph_clustering_similarity', host="localhost", port=45432)
    if verbose:
        print(datetime.datetime.now(), 'organize clusters - start')
    data = db.get_clusters(['clusters', 'graph'])
    grouped = group_all_clusters(data, db)
    db.disconnect()
    if verbose:
        print(datetime.datetime.now(), 'organize clusters - end')
    return grouped


if in_ipynb() and __name__ == "__main__":
    organize_graphs()


# %%
import sys

if __name__ == "__main__":
    if 'calc_clusters' in sys.argv:
        dataset = init(sys.argv[-2], sys.argv[-1])
        calc_container(dataset, sys.argv[-2])
    if 'draw_graph' in sys.argv:
        draw_graph(sys.argv[-3], sys.argv[-2], sys.argv[-1])
    if 'organize_graphs' in sys.argv:
        organize_graphs(sys.argv[-1])
