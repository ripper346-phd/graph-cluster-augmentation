import sys
import clusterize
import similarity
import swap_clusters

if __name__ == "__main__":
    if 'calc_clusters' in sys.argv:
        dataset = clusterize.init(sys.argv[-2], sys.argv[-1])
        clusterize.calc_container(dataset, sys.argv[-2])
    if 'draw_graph' in sys.argv:
        clusterize.draw_graph(sys.argv[-3], sys.argv[-2], sys.argv[-1])
    if 'organize_graphs' in sys.argv:
        clusterize.organize_graphs(sys.argv[-1])
    if 'compute_similarity' in sys.argv:
        similarity.compute_similarity(sys.argv[-1])
    if 'compute_permutations' in sys.argv:
        similarity.compute_permutations(sys.argv[-1])
    if 'calc_hks' in sys.argv:
        swap_clusters.calc_hks_graph(sys.argv[-2], sys.argv[-1], show_graph=True)
    if 'save_hks' in sys.argv:
        swap_clusters.save_hks_signatures(sys.argv[-1], True)
    if 'swap' in sys.argv:
        swap_clusters.swap(sys.argv[-3], sys.argv[-2], sys.argv[-1], True, True)
    if 'swap_all' in sys.argv:
        swap_clusters.swap_all(sys.argv[-1])
    if 'swap_n' in sys.argv:
        print(swap_clusters.swap_n(sys.argv[-2], int(sys.argv[-1])))
    if 'full_cluster' in sys.argv:
        dataset = clusterize.init(sys.argv[-2], sys.argv[-1])
        clusterize.calc_container(dataset, sys.argv[-2])
        clusterize.organize_graphs(sys.argv[-2], True)
        swap_clusters.save_hks_signatures(sys.argv[-2], True)
