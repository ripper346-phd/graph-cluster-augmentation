import numpy as np
import json


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if type(obj) == np.matrix or type(obj) == np.ndarray:
            return np.squeeze(np.asarray(obj)).tolist()
        elif np.issubdtype(obj, np.integer):
            return int(obj)
        elif np.issubdtype(obj, np.floating):
            return float(obj)
        elif np.issubdtype(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(np.EncodeNumpy, self).default(obj)
