from typing import List, Union
import numpy as np
from numpy.core.fromnumeric import transpose


def auction_alg(matrix: np.ndarray, force_final_list: bool = False, full_association=False) -> List[Union[int, List[int]]]:
    """Auction algorithm extended to not square matrices.

    Args:
        matrix (np.ndarray): numpy matrix of numbers to associate row-columns
        force_final_list (bool, optional): force the return of a list of lists of int. Defaults to False.
        full_association (bool, optional): Guarantee that every node is associated to another in non square matrices. Defaults to False.

    Returns:
        List: list of associations. If the matrix is square or the rows are more than columns it returns a list of int.
            If the columns are more than rows it return a list of lists of int, due the duplication.
    """
    assert(not np.isnan(np.sum(matrix)))
    transposed = False
    if full_association:
        if matrix.shape[0] < matrix.shape[1]:
            matrix = matrix.transpose()
            transposed = True
    else:
        if matrix.shape[0] > matrix.shape[1]:
            matrix = matrix.transpose()
            transposed = True

    agents = [None] * matrix.shape[0]
    agents_ghost = [None] * matrix.shape[0]
    prices = [0] * matrix.shape[1]

    while None in agents:
        unassigned_agent = agents.index(None)
        agents_row = matrix[unassigned_agent]

        max_diff = 0
        max_obj = 0
        next_max_diff = 0
        for j in range(matrix.shape[1]):
            value = agents_row[j] - prices[j]
            if value > max_diff:
                next_max_diff = max_diff
                max_diff = value
                max_obj = j

        # Remove old assignment. If it is reassigning the same value of ghost it doesn't remove the counterpart
        # although it would be an infinite loop.
        if not full_association or agents_ghost[unassigned_agent] != max_obj:
            for key, value in enumerate(agents):
                if value == max_obj:
                    agents_ghost[key] = agents[key]
                    agents[key] = None

        agents[unassigned_agent] = max_obj
        # Adding epsilon to price
        prices[max_obj] += max_diff - next_max_diff

    # Invert agents if the matrix was transposed in the beginning
    if transposed:
        return flip_array(agents, matrix.shape[1], force_final_list)
    if force_final_list:
        return [[idx] for idx in agents]
    return agents


def flip_array(arr, size=None, force_final_list: bool = False):
    flipped = {}
    for key, agent in enumerate(arr):
        if isinstance(agent, list):
            for agent2 in agent:
                if not flipped.get(agent2):
                    flipped[agent2] = []
                flipped[agent2].append(key)
        else:
            if not flipped.get(agent):
                flipped[agent] = []
            flipped[agent].append(key)
    arr = []
    if not size:
        size = len(flipped.keys())
    for i in range(size):
        if force_final_list and flipped.get(i) is None:
            arr.append([])
        else:
            arr.append(flipped.get(i))
    return arr
