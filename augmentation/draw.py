from matplotlib import pyplot, patches
import networkx as nx


def draw_adjacency_matrix(G, node_order=None, partitions=[], colors=[], title=None, point_to_pixel=True):
    """Draw adjacency matrix of graph

    Args:
        G (networkX graph | numpy array/matrix): graph to draw. It can be passed also an adjacency matrix
        node_order (list, optional): list of nodes, where each node in G appears exactly once. Defaults to None.
        partitions (list, optional): list of node lists, where each node in G appears in exactly one node list. Defaults to [].
        colors (list, optional): list of strings indicating what color each partition should be.
            If partitions is specified, the same number of colors needs to be specified. Defaults to [].
        title (string, optional): title to display.
    """
    if type(G) is nx.Graph:
        adjacency_matrix = nx.to_numpy_array(G, nodelist=node_order)
    else:
        adjacency_matrix = G

    # Plot adjacency matrix in toned-down black and white
    pyplot.figure(figsize=(10, 10), dpi=72)  # in inches
    pyplot.imshow(adjacency_matrix, cmap="Greys", interpolation="none")

    # The rest is just if you have sorted nodes by a partition and want to
    # highlight the module boundaries
    assert len(partitions) == len(colors)
    ax = pyplot.gca()
    if point_to_pixel:
        set_size(adjacency_matrix.shape[0], adjacency_matrix.shape[0], ax, dpi=72)
    if title:
        ax.set_title(title)
    for partition, color in zip(partitions, colors):
        current_idx = 0
        for module in partition:
            ax.add_patch(patches.Rectangle((current_idx, current_idx),
                                           len(module),  # Width
                                           len(module),  # Height
                                           facecolor="none",
                                           edgecolor=color,
                                           linewidth="1"))
            current_idx += len(module)
    pyplot.show(block=False)


def set_size(width, height, ax=None, unit='pixel', dpi=None):
    """Set size of figure respecting the size of the area

    Args:
        width (number): width to set
        height (number): height to set
        ax (Axes, optional): axes of the figure. Defaults to None.
        unit (str, optional): unit of the dimensions. Defaults to 'pixel'.
        dpi (int, optional): dpi of the figure. Defaults to None.
    """
    if not ax:
        ax = pyplot.gca()
    if unit == 'pixel':
        dpi = dpi if dpi is not None else ax.figure.dpi
        width /= dpi
        height /= dpi
    l = ax.figure.subplotpars.left
    r = ax.figure.subplotpars.right
    t = ax.figure.subplotpars.top
    b = ax.figure.subplotpars.bottom
    fig_width = float(width) / (r - l)
    fig_height = float(height) / (t - b)
    ax.figure.set_size_inches(fig_width, fig_height)
