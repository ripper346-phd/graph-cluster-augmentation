def in_ipynb():
    try:
        get_ipython().config
        return True
    except NameError:
        return False
