from augmentation.database import Database
import numpy as np
from grakel.kernels import WeisfeilerLehman, VertexHistogram
from grakel import graph_from_networkx as to_grakel_graph
from datetime import datetime
from multiprocessing.pool import ThreadPool


def similarity(cluster1, cluster2):
    # Uses the Weisfeiler-Lehman subtree kernel to generate the kernel matrices
    gk = WeisfeilerLehman(n_iter=4, base_graph_kernel=VertexHistogram, normalize=True)
    gk.fit(cluster1)
    K = gk.transform(cluster2)
    return K


def calc_similarity(cluster1, cluster2, reverse=True):
    gk1 = to_grakel_graph(cluster1, node_labels_tag='degree_labels')
    gk2 = to_grakel_graph(cluster2, node_labels_tag='degree_labels')
    similarity_res = similarity(gk1, gk2)
    return (similarity_res, np.transpose(similarity_res) if reverse else None)


def similarity_from_graph_couple(data_container, start_idx, compare_idx, persistent_load=True, super_verbose=False):
    if super_verbose:
        print('comparing', start_idx, compare_idx)
    from_res, to_res = calc_similarity(
        data_container[start_idx]['subgraphs'],
        data_container[compare_idx]['subgraphs'],
        persistent_load
    )
    if persistent_load:
        data_container[start_idx]['wl_res'][compare_idx] = from_res
        data_container[compare_idx]['wl_res'][start_idx] = to_res
    return (start_idx, compare_idx, from_res)


def similarity_from_graph(data_container, graph_idx, start_idx, n_clusters=3, db: Database = None, try_load=False, persistent_load=True, verbose=False, super_verbose=False):
    if verbose:
        print('using', start_idx, datetime.now())
    if db and try_load:
        data_container[start_idx]['wl_res'] = db.get_graph_similarity(start_idx, n_clusters)
    if len(data_container[start_idx]['wl_res'].keys()) < len(graph_idx) - 1:
        ids_to_compare = [idx for idx in graph_idx if data_container[idx]['id'] > start_idx and
                          data_container[start_idx]['g_class'] == data_container[idx]['g_class']]
        similarity_threads = []
        similarity_pool = ThreadPool(processes=64)
        for compare_idx in ids_to_compare:
            if data_container[start_idx]['wl_res'].get(compare_idx) is None:
                similarity_threads.append(similarity_pool.apply_async(
                    similarity_from_graph_couple,
                    (data_container, start_idx, compare_idx, persistent_load, super_verbose)))
        indices_to_save = [res.get() for res in similarity_threads]
        if db and len(indices_to_save) > 0:
            db.save_similarity_block(indices_to_save, n_clusters, data_container)
            # db.save_similarity(compare_idx, start_idx, n_clusters, to_res.tolist())


def similarity_on_clusters(data_container, n_clusters=3, db: Database = None, try_load=False, persistent_load=True, verbose=False, super_verbose=False):
    data_container = {graph['id']: {**graph, 'wl_res': {}} for graph in data_container}
    pool = ThreadPool(processes=150)
    threads = []
    graph_idx = list(data_container.keys())
    for start_idx in graph_idx:
        threads.append(pool.apply_async(
            similarity_from_graph,
            (data_container, graph_idx, start_idx, n_clusters,
             db, try_load, persistent_load, verbose, super_verbose)
        ))
    [res.get() for res in threads]
